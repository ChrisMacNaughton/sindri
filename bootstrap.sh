#!/usr/bin/env bash

target_dir=$(mktemp -d)

function install_dependencies() {
  echo "Ensuring basic dependencies are available"
  if [[ $(uname) == "Linux" && -f $(which apt-get) ]]; then
    sudo apt-get update -qqq
    pip3 -h > /dev/null || sudo apt-get install --yes --quiet python3-dev python3-pip git
  fi

  if [[ ! $(which ansible) ]]; then
    sudo pip3 install ansible
  fi
}

function install_playbooks() {
  ansible-galaxy install -r $target_dir/requirements.yml --force
}

function run_ansible() {
  install_playbooks
  current_dir=$(pwd)
  cd $target_dir
  echo "Running Sindri"
  ansible-playbook -i hosts.yml -b --ask-become-pass playbook.yml --ask-vault-pass
  cd $current_dir
}

function fetch_sindri() {
  echo "Cloning Sindri"
  git clone -q https://gitlab.com/ChrisMacNaughton/sindri.git $target_dir
}

install_dependencies
fetch_sindri
run_ansible
